# PerformanceWorkshop

The App which demonstrates a cases of performance degradation and the ways of how performance can be improved.  

## Getting started

Start UI:<br>
`npm run start`

Start Backend:<br>
`ng serve backend`

